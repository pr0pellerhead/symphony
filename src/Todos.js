import React from 'react';

export const Todos = (props) => {
    return (
        <ul>
            {props.todos.map((t, i) => (<li key={i}>{t.title}</li>))}
        </ul>
    );
};

