import React from 'react';

class FirstInput extends React.Component {
    render() {
        return (
            <input value={this.props.email} onChange={this.props.onChange}/>
        );
    }
}

export default FirstInput;