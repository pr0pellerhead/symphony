import React from 'react';

class TodoForm extends React.Component {
    render () {
        return (
            <div>
                <input value={this.props.value} onChange={this.props.onChange} />
                <button onClick={this.props.onClickAdd}>+</button>
            </div>
        );
    }
}

export default TodoForm;