import React from 'react';

class FirstComponent extends React.Component {
    render () {
        return (
        <h2>First component {this.props.ime}</h2>
        );
    }
}

export default FirstComponent;