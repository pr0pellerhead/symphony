import React from 'react';

class TodoList extends React.Component {
    render () {
        return (
            <ul>
                {this.props.items.map((v, i) => {
                    return <TodoItem id={i} todo={v} key={i} removeTodo={this.props.removeTodo}/>
                })}
            </ul>
        );
    }
}

const TodoItem = (props) => {
    return (
        <li>
            <span>{props.todo}</span>
            <button onClick={() => {props.removeTodo(props.id)}}>&times;</button>
        </li>
    )
};

export default TodoList;