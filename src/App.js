import React from 'react';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import Ajrodrom from './Ajrodrom';
import {Todos} from './Todos';

class App extends React.Component {
  componentDidMount() {
    console.log('componentDidMount');
  }
  
  componentDidUpdate() {
    console.log('componentDidUpdate');
  }

  componentWillReceiveProps() {
    console.log('componentWillReceiveProps');
  }

  componentWillMount() { // DEPRECATED
    console.log('componentWillMount');
  }

  componentDidCatch() {
    console.log('componentDidCatch');
  }

  shouldComponentUpdate() {
    console.log('shouldComponentUpdate');
    return true;
  }

  constructor() {
    super();
    this.state = {
      todoitems: ['a', 'b', 'c'],
      value: '',
      todos: []
    };

    fetch('https://jsonplaceholder.typicode.com/todos')
    .then(res => {
      return res.json();
    })
    .then(data => {
      this.setState({
        todos: data
      });
    })
    .catch(err => {
      console.log(err);
    });
  }

  onChange = (e) => {
    this.setState({
      value: e.target.value,
    })
  }

  onClickAdd = (e) => {
    this.setState({
        todoitems: [...this.state.todoitems, this.state.value],
        value: ''
    });
  }

  removeTodo = (ti) => {
    this.setState({
      todoitems: this.state.todoitems.filter((v, i) => {
        if(i !== ti){
          return v;
        }
      })
    })
  }

  render() {
    return (
      <>
        <TodoForm value={this.state.value} onChange={this.onChange} onClickAdd={this.onClickAdd}/>
        <TodoList items={this.state.todoitems} removeTodo={this.removeTodo}/>
        <Ajrodrom />
        <Todos todos={this.state.todos} />
      </>
    );
  }
}

export default App;
